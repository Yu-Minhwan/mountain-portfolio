var express = require('express'),
    colors = require('colors'),
    config = require('config'),
    path = require('path');

var app = express(),
    server = require('http').createServer(app);

// Bootstrap Web and Api service
require(path.join(__dirname, 'app', 'bootstrap', 'bootstrap'))(app);

// set app service port
_port = config.get('port');

app.set('port', _port);

server.listen(_port, function(){
  console.log('# Server Booting success!'.red + '\t' + _port);
});

