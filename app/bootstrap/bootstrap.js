
module.exports = function(app, config){
  
  require('./database');

  // Express Engine
  require('./express')(app);

  // Routers
  require('../routers/works')(app);
  require('../routers/pages')(app);

  // Routers - adminpage
  require('../routers/admin/pages')(app);
  require('../routers/admin/api')(app);
};
