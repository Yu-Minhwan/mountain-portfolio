var mongoose = require('mongoose'),

    colors = require('colors'),
    config = require('config');

/* 
 * Mongo Database */
mongoose.connect(config.get('db.mongodb.host'));

var mongoDB = mongoose.connection;
mongoDB.on('err', function () {
  console.log('MongoDB Connection Fail'.bold.red);
});
mongoDB.on('open', function (callback) {
  console.log('MongoDB Connection Success'.bold.blue);
});

  
