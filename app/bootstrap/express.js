// NPMs
var favicon = require('serve-favicon'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    express = require('express'),
    config = require('config');

// Cross-Origin Reouse Sharing... (All access allow)
var CORS_all = function(req, res, next){
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  res.header('Access-Control-Allow-Credentials', true);
  next();
};

module.exports = function(app){
  // set view engine and view file's root path
  app.set('views', config.get('path.views'));
  app.set('view engine', 'jade');

  //app.use(favicon(config.get('path.public') + '/favicon.ico'));
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(express.static(config.path.public));
  

  /*
  app.use(function(req, res, next){
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  app.use(function(err, req, res, next){
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });*/

  
  app.use(CORS_all)
};
 

