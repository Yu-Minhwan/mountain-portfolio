
var controller = require('../controllers/pages.js');

module.exports = function(app){
  app.get('/', function(req, res){
  	res.redirect('/home');
  });

  app.get('/:page', controller.page);
}
