var fs = require('fs-extra'),
    formidable = require('formidable'),
    util = require('util'),
    config = require('config'),
    aws = require('aws-sdk'),
    async = require('async');

var controller = {
  'main_setting': require('../../controllers/admin/api/main_setting.js'),
  'works': require('../../controllers/admin/api/works.js'),
}

module.exports = function(app){
  app.post('/admin/api/works', controller['works'].upload);
  app.put('/admin/api/works', controller['works'].put);
  app.delete('/admin/api/works', controller['works'].delete);


  app.post('/admin/api/main_setting', controller['main_setting'].update);
}
