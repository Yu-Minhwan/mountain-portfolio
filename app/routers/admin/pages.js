
module.exports = function(app){
  app.get('/admin/:page', function(req, res){
	var page = req.params.page;
	res.render('admin/' + page, {'page': page});
  });
}
