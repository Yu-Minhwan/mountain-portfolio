var controller = require('../controllers/works.js');

module.exports = function(app){
  app.get('/home', controller.home);
  app.get('/works', controller.list);
  app.get('/detail/:title', controller.detail);
}
