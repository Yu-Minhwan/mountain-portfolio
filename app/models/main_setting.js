var config = require('config'),
    mongoose = require('mongoose');

var schema = new mongoose.Schema({
    "slide_images": Array,
    "updated": { type: Date, default: Date.now },
});

var model = mongoose.model('main_setting', schema);

exports.get = function(cb){
    model
    .findOne({})
    .sort({'updated': 'desc'})
    .exec(function (err, setting) {
        cb(err, setting);
    });
};

exports.new_setting = function(form, s3_images, cb){
    var fields = form['fields'];

    var doc = new model();

    doc.slide_images = s3_images;
    // for(idx in fields['slidE_image_order']){
    //     doc.slide_images.push(s3_images[fields['slidE_image_order'][idx]]);
    // }
    
    doc.save(function(err){
        cb(null, doc);
    })
}