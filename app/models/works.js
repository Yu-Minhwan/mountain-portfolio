var config = require('config'),
    mongoose = require('mongoose');

var schema = new mongoose.Schema({
    "title" : String,
    "desc" : String,
    "photographer" : String,
    "writer" : Array,
    "image" : String,
    "images": Array,
    "movie": String,
    "order" : Object,
    "size" : Number
});

var model = mongoose.model('works', schema);

exports.getAll = function(cb){
    model
    .find({})
    .sort({ 'order': 'asc'})
    .exec(function (err, works) {
        cb(err, works);
    });
};

exports.get = function(query, cb){
    model.findOne( query, function(err, work){
        cb(err, work);
    });
}

exports.put = function(form, s3_images, cb){
    var fields = form['fields'];

    var doc = new model();
    console.log(fields)
    doc.title = fields['title'].replace(/ /g, '-');
    doc.desc = fields['desc'];
    doc.photographer = fields['photographer'];
    doc.writer = fields['writer'];

    doc.images = s3_images;

    if(fields['movie']){
        var video_id = fields['movie'].split('v=')[1];
        var ampersandPosition = video_id.indexOf('&');
        if(ampersandPosition != -1) {
          video_id = video_id.substring(0, ampersandPosition);
        }
        doc.movie = video_id;
    }
    doc.size = Number(fields['size']);
    doc.orer = 1;

    doc.save(function(err){
        cb(null, doc);
    })
}