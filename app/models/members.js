var config = require('config'),
    mongoose = require('mongoose');

var schema = new mongoose.Schema({
    "name" : String,
    "position" : Array,
    "email" : String,
    "biography" : Array,
    "birth" : Object,
    "web" : Array,
    "sns" : Array,
    "order": Number
});

var model = mongoose.model('members', schema);

exports.getAll = function(cb){
    model
    .find({})
    .sort({ order: 'asc'})
    .exec(function (err, members) {
        cb(err, members);
    });
};

exports.get = function(memberObejctID, cb){
    model.findOne( {'_id': memberObejctID}, function(err, member){
        cb(err, member);
    });
}