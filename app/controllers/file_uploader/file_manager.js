var fs = require('fs-extra'),
    formidable = require('formidable'),
    util = require('util'),
    async = require('async');

exports.form_parse = function(req, cb){
	var form = new formidable.IncomingForm();

	var fields;
	form.parse(req, function(err, _fields, files){
		if(err) return cb(err);
		fields = _fields;
	});	

	form.on('end', function(_fields, _files){
		cb(null, {
			'fields': fields,
			'files': this.openedFiles,
		});
	});
}

exports.cp_formFile2server = function(files, dir, cb){
	if(dir === undefined || dir == null)
		dir = 'tmp/';

	var jobs = [];
	for(var idx in files){
		var curr_file = files[idx];
		
		jobs.push(async.apply(mv_file, 
			curr_file.path, dir, curr_file.name));
	}

	async.parallel(
		jobs,
		function(err, results){ 
			cb(err, results);
		}
	);	
}

function mv_file(tmpFile_path, dir, file_name, cb){
	var target_path = dir + '/' + file_name;

	fs.copy(tmpFile_path, target_path, function(err){
		cb(err, {
			'path': target_path,
			'name': file_name
		});
	});
}
exports.mv_file = mv_file;