var fs = require('fs-extra'),
	config = require('config'),
	async = require('async'),
    aws = require('aws-sdk');

aws.config.update({
  accessKeyId: config.get('aws.credential.accessKeyId'), 
  secretAccessKey: config.get('aws.credential.secretAccessKey'),
  regoin: "us-east-1",
  signatureVersion :"v4"
});
var s3 = new aws.S3();

function upload_files(files, remotedir, cb){
	/*
		files = {
			path: str
			name: str
		}
	*/
	var jobs = []

	for(var idx in files){
		file = files[idx];

		jobs.push(async.apply(upload, 
			file.path, remotedir + '/' + file.name));
	}

	async.parallel(jobs, function(err, results){
		cb(err, results);
	});
}
exports.upload_files = upload_files;

function upload(fileName, remoteFilename,  cb){
    var fileBuffer = fs.readFileSync(fileName);
    var metaData = getContentTypeByFile(fileName);
    
    s3.putObject({
      ACL: 'public-read',
      Bucket: config.get('aws.s3.bucket'),
      Key: remoteFilename,
      Body: fileBuffer,
      ContentType: metaData
    }, function(error, response) {
      cb(null, remoteFilename);
    });
}
exports.upload = upload;

function getContentTypeByFile(fileName) {
	var rc = 'application/octet-stream';
	var fn = fileName.toLowerCase();

	if (fn.indexOf('.html') >= 0) rc = 'text/html';
	else if (fn.indexOf('.css') >= 0) rc = 'text/css';
	else if (fn.indexOf('.json') >= 0) rc = 'application/json';
	else if (fn.indexOf('.js') >= 0) rc = 'application/x-javascript';
	else if (fn.indexOf('.png') >= 0) rc = 'image/png';
	else if (fn.indexOf('.jpg') >= 0) rc = 'image/jpg';

	return rc;
}