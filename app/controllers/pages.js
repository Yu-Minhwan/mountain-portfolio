var async = require('async');

var models = {
	'members': require('../models/members.js')
}

exports.page = function(req, res){
	var page = req.params.page;

	switch(page){
		case 'company':
			company_page(req, res);
			break;
		case 'contact':
			res.render('page/' + page, {'page': page});
			break;
		default:
			res.send('invalid request');
	}
}

function company_page(req, res){
	async.parallel([
		models['members'].getAll,
	], function(err, result){
		//TODO: error handling
		res.render('page/company', {
			'page': 'company',
			'members': result[0]
		})
	});
}