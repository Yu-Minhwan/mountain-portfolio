var async = require('async');

var models = {
	'main_setting': require('../models/main_setting'),
	'works': require('../models/works.js')
}


exports.home = function(req, res){
	async.parallel([
		models['main_setting'].get,
		models['works'].getAll,
	],function(err, result){
		var main_setting = result[0];
		var works = result[1];

		if(works.length > 5) result = result.slice(0,5);

		res.render('page/home', {
			'page': 'home',
			'slides': main_setting['slide_images'],
			'works': works
		});
	});
}

exports.list = function(req, res){
	async.waterfall([
		models['works'].getAll
	],function(err, result){
		res.render('works/list', {
			'page': 'works',
			'works': result,
		});
	});
}

exports.detail = function(req, res){
	var title = req.params.title;

	// get work
	models['works'].get({'title': title}, function(err, work){
		// get prev, next work
		async.parallel([
			async.apply(models['works'].get, {'order': work.order-1}),
			async.apply(models['works'].get, {'order': work.order+1})
		], function(err, results){
			work.title = title.replace(/-/g, ' ');
			res.render('works/detail', {
				'page': 'works',
				'work': work,
				'prev': results[0],
				'next': results[1],
				'title': title
			});
		});
	})
}