var async = require('async'),
    fm = require('../../file_uploader/file_manager'),
    s3 = require('../../file_uploader/s3'),
    model = require('../../../models/main_setting');


exports.update = function(req, res){
  var form;

  //TODO: pass params (all configuration: path, form-files, fields)
  async.waterfall([
    // form data - > local
    async.apply(fm.form_parse, req),
    function(_form, cb){
      form = _form;
      cb(null, form['files'], 'tmp')
    },
    fm.cp_formFile2server, 
    // local -> s3
    function(files, cb){
      cb(null, files, 'main-slide')
    },
    s3.upload_files,
    // insert database
    function(s3_images, cb){
      cb(null, form, s3_images);
    },
    model.new_setting
  ], function(err, result){
    res.json(result);
  })
};
