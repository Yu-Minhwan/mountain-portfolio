$(window).load(function(){
	var $grid = $('.grid').masonry({
	  itemSelector: '.grid-item',
	  percentPosition: true,
	  columnWidth: '.grid-sizer'
	});

	// layout Isotope after each image loads
	$grid.imagesLoaded().progress( function(instance, image) {
		$image = $(image.img);
		if($image.attr('class') == 'square'){
			w = $image.width();
			$image.css('height', w);
		}

	  	$grid.masonry();
	}); 

	// grid cell hover -> fadeIn cover
	var animateSpeed = 400;
	$('.grid-item .cover').on({
		'mouseenter': function(){
			event.preventDefault();

			$(this).animate(
				{'opacity': 1.0}, 
				animateSpeed,
				function(){ isAnimated = false; }
			);
		},
		'mouseleave': function(){
			event.preventDefault();

			$(this).animate(
				{'opacity': 0.0},
				animateSpeed,
				function(){ isAnimated = false; }
			);
		}
	});


	$(window).resize(function(){
		var grids = $('.grid-item');

		$.each(grids, function(idx, grid){
			$image = $(grid).find('img.square');

			if($image.attr('class') == 'square'){
				w = $image.width();
				$image.css('height', w);
			}
		});
	})
});