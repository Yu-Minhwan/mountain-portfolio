/* The configuration of map */
var MAP_CONFIG = {
  'center': new google.maps.LatLng(37.534533, 126.992385),
  'selector' : 'map',
  'option'  : {
    zoom: 15,
    scaleControl: true,
    scaleControlOptions: {
      position: google.maps.ControlPosition.TOP_LEFT
    },
    mapTypeControl: false,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
      position: google.maps.ControlPosition.TOP_CENTER
    },
    streetViewControl: false,
    zoomControl: false,
    panControl: false,
  },
  'style' : [
    {
      stylers: [
        { hue: "#00ffe6" },
        { saturation: -20 }
      ]
    },{
      featureType: "road",
      elementType: "geometry",
      stylers: [
        { lightness: 100 },
        { visibility: "simplified" }
      ]
    },{
      featureType: "road",
      elementType: "labels",
      stylers: [
        { visibility: "off" }
      ]
    }
  ]
};


function rendering_map(){
	var map = new google.maps.Map(document.getElementById(MAP_CONFIG['selector']), MAP_CONFIG['option']);
	map.setOptions({styles: MAP_CONFIG['style']});

	//37.534533,126.992385,17z
	pos = MAP_CONFIG['center'];
	map.panTo(pos);

	var marker = new google.maps.Marker({
        position: MAP_CONFIG['center'],
        map: map,
        icon: 'http://www.googlemapsmarkers.com/v1/M/000000/FFFFFF/ebebeb',
        animation: google.maps.Animation.DROP,
      });
	marker.setMap(map);
}