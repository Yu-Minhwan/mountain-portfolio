
$(function(){

  function hide_footer(){
    $('#footer').hide();
  }

  // menu initializer
  function initialize_menu(){
    $target = $('#menu li');
    
    // add Slash
   //  $.each($target, function(idx, menu){
   //  	if(idx != $target.length -1)
			// $('<li class="slash">/</li>').insertAfter($(menu));
   //  });
        
    // activated menu
   $('#menu li[value=\'' + PAGE['name'] + '\']').addClass('active');
  }

  initialize_menu();

  switch(PAGE.name){
    case 'contact':
    case 'company':
      hide_footer();
      break;
  }
})



$(window).load (function(){
  function page_initializer(){
    fit2width();
    fit_grid();

    switch(PAGE['name']){
      case 'home':
        image_slider = new IMAGE_SLIDE('#home .image-slide');
        image_slider.start();
        break;
      case 'company':
        remove_lastElement();
        break_firsthSpace();

        image_slider = new IMAGE_SLIDE('#company .image-slide');
        image_slider.start();
        break;
      case 'contact':
        rendering_map();
        break;
    }


    $('.row').hide().fadeIn(1500);
    $('.grid').hide().fadeIn(1500);
  }

  // resize grid cell(fit to parent wrapper)
  function fit_grid(){
  	var cells = $('.row > .fit-parent');

  	$.each(cells, function(idx, cell){
  		$cell = $(cell);
	  	var parent = $cell.parent();

	  	$cell.css('height', parent.height());
  	});
  }

  // text utils
  function remove_lastElement(){
  	$.each($('.rm-last'), function(idx, val){
  		$(val).children().last().remove();
  	});
  }

  function break_firsthSpace(){
  	$.each($('.break-first-space'), function(idx, val){
  		text = $(val).text();

  		idx = text.indexOf(' ');
  		if(idx == -1) return;

  		html = text.substring(0,idx) + '<br>' + text.substring(idx);
  		$(val).html(html);
  	});
  }

  $("#toTopBtn").click(function() {
    $("html, body").animate({ scrollTop: 0 });
    return false;
  });

  $(window).resize(function(){
    fit_grid();
  })

  $(window).scroll(function(){
    _top = $(window).scrollTop();
    
    if(_top > 400){
      $('.work-indicator').fadeIn();
    }
    else{
      $('.work-indicator').fadeOut();
    }
  })

  page_initializer();
});
