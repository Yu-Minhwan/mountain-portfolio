IMAGE_SLIDE = function(target, images){
	this.$target = $(target);
	this.images = $('.image-slide img');
	console.log(this.images);
	this.current_idx = 0;
}

IMAGE_SLIDE.prototype.start = function(){
    fit_homeSlider();
	this.$target.find('img').hide();
	$(this.$target.find('img')[0]).show();

	m = this;
	setInterval(function(){
		m.change_next(m)
	}, 4000);
}

IMAGE_SLIDE.prototype.change_next = function(m){
	next_idx = m.current_idx + 1 <= m.images.length-1 ?
		m.current_idx + 1 : 0;

	m.change(m, next_idx);
}

IMAGE_SLIDE.prototype.change = function(m, idx){
	fit_homeSlider();
	m.$target.find('img').fadeOut(1000);
	$(m.$target.find('img')[idx]).fadeIn(1000);
	m.current_idx = idx;
}

// grid cell
function fit2width(){
	$.each($('.square'), function(idx, val){
	  _w = $(".grid-sizer").width();
	  $(val).css('min-height', _w);
	});
}

function fit_homeSlider(){
	$slide = $("#home .image-slide")
	 _w = $slide.width();
	$slide.css('min-height', _w*3/5);

	$.each( $('#home .image-slide img'), function(idx, image){
	 	$image = $(image);
		h = $image.height();
		$image.css('margin-top', -1*h/2);
	});
}

$(function(){
  $(window).resize(function(){
  	fit_homeSlider();
	fit2width();
  });
})
  
