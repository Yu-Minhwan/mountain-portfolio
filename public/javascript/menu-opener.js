$(function(){
	$opener = $('.loader');
	$menu = $('#menu');

	$fixed_opener = $opener.clone();
	$fixed_menu = $menu.clone();

	$fixed_opener.addClass('fixed').addClass('menu-opener').appendTo('body');
	$fixed_menu.addClass('fixed').hide().appendTo('body');

	$fixed_opener.hide();

	var status = false;
	$('.menu-opener').click(function(){
		if(status)
			$fixed_menu.slideUp();
		else
			$fixed_menu.slideDown();

		status = status ? false : true;
	})

	$(window).scroll(function(){
    	_top = $(window).scrollTop();

	    if(_top > 550){
	      $('.menu-opener').fadeIn();
	    }
	    else{
	      $('.menu-opener').fadeOut();
	    }
  })
})
